module.exports = function(grunt) {

  grunt.initConfig({
    pug: {
      compile: {
        options: {
          data: {
            debug: false
          },
          pretty: true
        },
        files: [
          {
            cwd: "./",
            src: "templates/**/*.pug",
            dest: "public/",
            expand: true,
            ext: ".html"
          }
        ],
      }
    },
    sass: {
      options: {
        sourceMap: true
      },
      dist: {
        files: {
          'public/css/style.css' : 'sass/style.sass'
        }
      }
    },
    copy: {
      main: {
        expand: true,
        src: ['index.html'],
        dest: 'public/',
        cwd: './public/templates'
      }
    },
    watch: {
      css: {
        files: 'sass/*.sass',
        tasks: ['sass'],
        options: {
          livereload: true,
        },
      },
      pug: {
        files: 'templates/**/*.pug',
        tasks: ['pug', 'copy'],
        options: {
          livereload: true,
        }
      }
    }
  });
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-sass');
  grunt.loadNpmTasks('grunt-contrib-pug');
  grunt.loadNpmTasks('grunt-contrib-copy');

  grunt.registerTask('default', ['pug', 'sass', 'copy', 'watch']);

};
