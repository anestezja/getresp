console.log('hello')

window.onload = function() {
/* toggle menu */
  const BUTTON = document.querySelector("#userMenuButton");
  const DROPDOWN_MENU_LINKS = document.querySelectorAll('.dropdownMenu > li > a');

  function toggleMenu() {
    const DROPDOWN_CONTAINER_CLASSES = BUTTON.parentElement.classList;
    DROPDOWN_CONTAINER_CLASSES.toggle('active')
  };

  BUTTON.addEventListener('click', toggleMenu);
  DROPDOWN_MENU_LINKS.forEach(function(link) {
    link.addEventListener('click', toggleMenu);
  });

  /* main menu mobile button */
  const MENU_BUTTON = document.querySelector('#menuButton');

  function togglePrimaryMenu() {
    const MENU = document.querySelector('.MAIN_MENU');
    (MENU.classList.contains('open'))? MENU.classList.remove('open') : MENU.classList.add('open');
  };

  MENU_BUTTON.addEventListener('click', togglePrimaryMenu);

  /* food types */
  const FOOD_BUTTONS = document.querySelectorAll('ul.options > li > button');

  function toggleFoodChoice() {
    const CLASSES = this.classList;
    if (CLASSES.contains('active')) {
      CLASSES.remove('active');
      CLASSES.add('blocked');
    } else {
      CLASSES.add('active');
      CLASSES.remove('blocked');
    }
  };

  FOOD_BUTTONS.forEach(function(button) {
    button.addEventListener('click', toggleFoodChoice);
  })

  /* week slider */
  const SLIDES = document.querySelectorAll('.text > ul > li');
  const BUTTON_PREV_SLIDE = document.querySelector('button.prev');
  const BUTTON_NEXT_SLIDE = document.querySelector('button.next');
  let current = 0;

  function setSlider(current) {
    SLIDES[current].classList.add('active');
    if(current == SLIDES.length - 1) {
      BUTTON_NEXT_SLIDE.disabled = true;
    } else if (current == 0) {
      BUTTON_PREV_SLIDE.disabled = true;
    } else {
      BUTTON_PREV_SLIDE.disabled = false;
      BUTTON_NEXT_SLIDE.disabled = false;
    }
  };
  function nextSlide() {
    if(current < SLIDES.length - 1) {
      SLIDES[current].classList.remove('active');
      current += 1;
      setSlider(current);
    }
  };
  function prevSlide() {
    if(current != 0) {
      SLIDES[current].classList.remove('active');
      current -= 1;
      setSlider(current);
    }
  };
  setSlider(0);
  BUTTON_NEXT_SLIDE.addEventListener('click', nextSlide);
  BUTTON_PREV_SLIDE.addEventListener('click', prevSlide);
}
